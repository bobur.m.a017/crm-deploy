<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('output_products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('username');
            $table->string('client_phone_number')->nullable();
            $table->string('client_name')->nullable();
            $table->boolean('debt')->default(false);
            $table->double('count');
            $table->foreignId('shop_id')->constrained();
            $table->foreignId('product_id')->constrained();
            $table->foreignId('output_product_total_id')->constrained();
            $table->double('price');
            $table->double('price_debt')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('output_products');
    }
};
