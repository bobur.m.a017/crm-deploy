<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            "name" => "Sherzod",
            "username" => "crm-lite-7777",
            "role_id" => 2,
            "password" => Hash::make("crm-lite-7777")
        ]);
        User::create([
            "name" => "Admin",
            "username" => "crm-lite-admin",
            "role_id" => 1,
            "password" => Hash::make("crm-lite-7777")
        ]);
        User::create([
            "name" => "Foydalanuvchi",
            "username" => "crm-lite-foydalanuvchi",
            "role_id" => 3,
            "password" => Hash::make("crm-lite-7777")
        ]);
    }
}
