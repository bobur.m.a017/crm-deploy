<?php

namespace Database\Seeders;

use App\Models\Shop;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Shop::create([
            "name"=>"Kiyimlar do'koni",
            "user_id"=>1,
        ]);
        Shop::create([
            "name"=>"Oziq-ovqatlar do'koni",
            "user_id"=>1,
        ]);
    }
}
