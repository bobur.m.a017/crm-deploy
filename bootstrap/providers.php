<?php


return [
    App\Providers\AppServiceProvider::class,
    App\Providers\ProviderServicesRepository::class,
    App\Providers\TelescopeServiceProvider::class,
];
