<?php
namespace App\Interfaces;

use App\Http\Requests\StoreOutputProductRequest;
use App\Http\Requests\UpdateOutputProductRequest;
use App\Models\OutputProduct;

interface OutputProductInterface
{
    public function getAllOutputProductBySHope(int $shopeId,string $startDate, string $endDate);
    public function getOutputProductById(int $OutputProductId);
    public function deleteOutputProduct(OutputProduct $product);
    public function createOutputProduct($storeProductRequest,int $outpuProductTotal,int $shopId);
    public function updateOutputProduct(UpdateOutputProductRequest $storeProductRequest,OutputProduct $product);
}
