<?php
namespace App\Interfaces;

use App\Http\Requests\StoreCategoryRequest;

interface CategoryInterface
{
    public function getAllCategoryBySHope(int $shopeId);
    public function getCategoryById(int $categoryId);
    public function getCategoryListByShopId(int $categoryId,string $search);
    public function deleteCategory(int $categoryId);
    public function createCategory(StoreCategoryRequest $storeCategoryRequest);
    public function updateCategory(int $categoryId,string $name);
}
