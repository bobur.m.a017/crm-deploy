<?php
namespace App\Interfaces;

use App\Http\Requests\StoreInputProductRequest;
use App\Http\Requests\UpdateInputProductRequest;
use App\Models\InputProduct;

interface InputProductInterface
{
    public function getAllInputProductBySHope(int $shopeId, string $start_date, string $end_date);
    public function getInputProductById(int $inputProductId);
    public function deleteInputProduct(InputProduct $product);
    public function createInputProduct(StoreInputProductRequest $storeProductRequest);
    public function updateInputProduct(UpdateInputProductRequest $storeProductRequest,InputProduct $product);
}
