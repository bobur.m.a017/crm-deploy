<?php
namespace App\Interfaces;

use App\Http\Requests\StoreProductRequest;
use App\Models\Product;

interface ProductInterface
{
    public function getAllProductBySHope(int $shopeId,string $name);
    public function getProductById(int $productId);
    public function deleteProduct(int $productId);
    public function getProductsListByShopId(int $shopId, string $name);
    public function createProduct(StoreProductRequest $storeProductRequest);
    public function updateProduct(StoreProductRequest $storeProductRequest,Product $product);
}
