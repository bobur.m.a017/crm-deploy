<?php
namespace App\Interfaces;

use App\Http\Requests\StoreStatisticsRequest;

interface StatisticsInterface
{
    public function getToatalStatisticsBySHope(int $shopeId,string $startDate,string $endDate);
    public function getWeeklyStatisticsBySHope(int $shopeId,string $startDate,string $endDate);
    public function getTopProductsStatisticsBySHope(int $shopeId,string $startDate,string $endDate);
    public function getDebtsStatisticsBySHope(int $shopeId);
    public function getResidualsStatisticsBySHope(int $shopeId);
   
}
