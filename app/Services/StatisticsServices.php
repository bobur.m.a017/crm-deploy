<?php

namespace App\Services;

use App\Http\Requests\StoreStatisticsRequest;
use App\Interfaces\StatisticsInterface;
use DateTime;
use Illuminate\Database\Eloquent\Collection;

class StatisticsServices
{
    protected StatisticsInterface $categroyInterface;
    public function __construct(StatisticsInterface $categroyInterface1)
    {
        $this->categroyInterface = $categroyInterface1;
    }

    public function getTotalStatistics(int $shopId, string $startDate, string $endDate)
    {
        return $this->categroyInterface->getToatalStatisticsBySHope($shopId, $startDate, $endDate);
    }
    public function getTopProductsStatisticsBySHope(int $shopId, string $startDate, string $endDate)
    {
        return $this->categroyInterface->getTopProductsStatisticsBySHope($shopId, $startDate, $endDate);
    }
    public function getResidualsStatisticsBySHope(int $shopId)
    {
        return $this->categroyInterface->getResidualsStatisticsBySHope($shopId);
    }
    public function getWeeklyStatisticsBySHope(int $shopId, int $year, int $moth)
    {

        $dayList = $this->getDaysInMonth($year, $moth);
        $startDate = $dayList[0];
        // dd($dayList);
        $list = array();
        $count = 0;
        foreach ($dayList as $key =>$value) {
            $count++;
            if ($count != 1 && $count % 7 == 1) {
                $result = $this->categroyInterface->getWeeklyStatisticsBySHope($shopId, $startDate, $value);
                array_push($list, $result);
                $startDate = $value;
            }
        }
        // dd($list,$count2);
        return $list;
    }
    public function getDaysInMonth($year, $month)
    {
        // Create a DateTime object for the first day of the month
        $date = new DateTime("{$year}-{$month}-01");

        // Get the number of days in the month
        $daysInMonth = $date->format('t');

        // Initialize an empty array to store dates
        $daysList = [];

        // Loop through each day in the month
        for ($i = 1; $i <= $daysInMonth; $i++) {
            // Create a string representation of the date in yyyy-mm-dd format
            $dayString = $date->format("Y-m-d");

            // Add the date string to the list
            $daysList[] = $dayString;

            // Move to the next day
            $date->modify('+1 day');
        }

        // Return the list of days
        return $daysList;
    }
}
