<?php

namespace App\Services;

use App\Http\Requests\StoreInputProductRequest;
use App\Http\Requests\UpdateInputProductRequest;
use App\Interfaces\InputProductInterface;
use App\Models\InputProduct;

class InputProductServices
{
    protected $InputproductInterface;
    protected $productServices;
    public function __construct(
        InputProductInterface $InputproductInterface1,
        ProductServices $productServices
    ) {
        $this->InputproductInterface = $InputproductInterface1;
        $this->productServices = $productServices;
    }

    public function getAllInputProductBySHope(int $shopeId, string $start_date, string $end_date)
    {
        return $this->InputproductInterface->getAllInputProductBySHope($shopeId, $start_date, $end_date);
    }
    public function getInputProductById(int $productId)
    {
    }
    public function deleteInputProduct(InputProduct $inputProduct)
    {
        if (!$this->productServices->calculateProductInputMinus($inputProduct)) {
            return false;
        }
        return $this->InputproductInterface->deleteInputProduct($inputProduct);
    }
    public function createInputProduct(StoreInputProductRequest $storeProductRequest): bool
    {
        $inputProduct = $this->InputproductInterface->createInputProduct($storeProductRequest);
        return $this->productServices->calculateProductInput($inputProduct);
    }

    public function updateInputProduct(UpdateInputProductRequest $storeProductRequest, InputProduct $product): bool
    {
        if (!$this->productServices->calculateProductInputMinus($product)) {
            return false;
        }
        $inputProduct = $this->InputproductInterface->updateInputProduct($storeProductRequest, $product);
        return $this->productServices->calculateProductInput($inputProduct);
    }
}
