<?php

namespace App\Services;

use App\CustomComponents\FromResponse;
use App\Http\Requests\StoreOutputProductRequest;
use App\Http\Requests\UpdateOutputProductRequest;
use App\Interfaces\OutputProductInterface;
use App\Models\OutputProduct;
use App\Models\OutputProductTotal;

class OutputProductServices
{
    protected $OutputproductInterface;
    protected $productServices;
    public function __construct(
        OutputProductInterface $OutputproductInterface1,
        ProductServices $productServices
    ) {
        $this->OutputproductInterface = $OutputproductInterface1;
        $this->productServices = $productServices;
    }

    public function getAllOutputProductBySHope(int $shopeId,string $startDate, string $endDate)
    {
        return $this->OutputproductInterface->getAllOutputProductBySHope($shopeId,$startDate,$endDate);
    }
    public function getOutputProductById(int $productId)
    {
    }
    public function deleteOutputProduct(OutputProduct $OutputProduct)
    {
        $this->productServices->calculateOutpuProductPlus($OutputProduct);
        return $this->OutputproductInterface->deleteOutputProduct($OutputProduct);
    }
    public function createOutputProduct(StoreOutputProductRequest $storeProductRequest): FromResponse
    {

        $outputProductTotal = OutputProductTotal::create([
            "shop_id" => $storeProductRequest->shop_id
        ]);
        $OutputProduct = $this->OutputproductInterface->createOutputProduct(
            $storeProductRequest->output_products, $outputProductTotal->id,$storeProductRequest->shop_id
        );
        return $this->productServices->calculateOutpuProductListMinus($OutputProduct);
    }

    public function updateOutputProduct(UpdateOutputProductRequest $storeProductRequest, OutputProduct $product): FromResponse
    {
        $reponse = $this->productServices->calculateOutpuProductPlus($product);
        if (!$reponse->state) {
            return $reponse;
        }
        $OutputProduct = $this->OutputproductInterface->updateOutputProduct($storeProductRequest, $product);
        
        return $this->productServices->calculateOutpuProductMinus($OutputProduct);
    }
}
