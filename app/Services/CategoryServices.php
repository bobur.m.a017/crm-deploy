<?php

namespace App\Services;

use App\Http\Requests\StoreCategoryRequest;
use App\Interfaces\CategoryInterface;
use Illuminate\Database\Eloquent\Collection;

class CategoryServices
{
    protected CategoryInterface $categroyInterface;
    public function __construct(CategoryInterface $categroyInterface1)
    {
        $this->categroyInterface = $categroyInterface1;
    }

    public function getCategories(int $shopId)
    {
        return $this->categroyInterface->getAllCategoryBySHope($shopId);
    }
    public function createCategory(StoreCategoryRequest $storeCategoryRequest)
    {
        return $this->categroyInterface->createCategory($storeCategoryRequest);
    }
    public function getCategoryListByShopId(int $shopId,string $search)
    {
        return $this->categroyInterface->getCategoryListByShopId($shopId,$search);
    }
}
