<?php

namespace App\Services;

use App\CustomComponents\FromResponse;
use App\Http\Requests\StoreProductRequest;
use App\Interfaces\ProductInterface;
use App\Models\InputProduct;
use App\Models\OutputProduct;
use App\Models\Product;

class ProductServices
{
    protected $productInterface;
    public function __construct(ProductInterface $productInterface1)
    {
        $this->productInterface = $productInterface1;
    }

    public function getProducts(int $shopId, string $name)
    {
        return $this->productInterface->getAllProductBySHope($shopId, $name);
    }
    public function getProductsListByShopId(int $shopId, string $name)
    {
        return $this->productInterface->getProductsListByShopId($shopId, $name);
    }
    public function createProduct(StoreProductRequest $storeProductRequest)
    {
        return $this->productInterface->createProduct($storeProductRequest);
    }
    public function calculateProductInput(InputProduct $inputProduct): bool
    {
        $product = Product::where("shop_id", $inputProduct->shop_id)->where("id", $inputProduct->product_id)->first();
        if ($product) {
            $inputProductTotalPrice = bcmul($this->checkOne($inputProduct->count), $this->checkOne($inputProduct->price, 6));
            $productTotalPrice = bcmul($this->checkOne($product->price), $this->checkOne($product->count), 6);
            $totalCount = bcadd($inputProduct->count, $product->count, 6);
            $product->price = bcdiv(bcadd($inputProductTotalPrice, $productTotalPrice, 6), $totalCount, 6);
            $product->count = $totalCount;
            $product->update();
            return true;
        } else {
            return false;
        }
    }
    public function calculateOutpuProductListMinus(array $outputProducts): FromResponse
    {
        foreach ($outputProducts as $key => $value) {
            $product = Product::where('id', $value["product_id"])->first();
            // dd($product);
            $ok = $this->checkTotalCountOrPrice($product->count, $value["count"]);
            $product->count = $ok ? $product->count - $value["count"] : 0;
            $product->update();
        }

        if ($ok) {
            return new FromResponse(true, "Muvafaqqiyatli bajarildi", 200);
        }
        return new FromResponse(false, "Muvafaqqiyatli bajarildi ammo mahsulot yetmadi", 200);
    }
    public function calculateOutpuProductMinus(OutputProduct $outputProducts): FromResponse
    {
        $product = Product::where('id', $outputProducts->product_id)->first();
        // dd($product);
        $ok = $this->checkTotalCountOrPrice($product->count, $outputProducts->count);
        $product->count = $ok ? $product->count - $outputProducts->count : 0;
        $product->update();

        if ($ok) {
            return new FromResponse(true, "Muvafaqqiyatli bajarildi", 200);
        }
        return new FromResponse(false, "Muvafaqqiyatli bajarildi ammo mahsulot yetmadi", 201);
    }
    public function calculateOutpuProductPlus(OutputProduct $outputProduct): FromResponse
    {
        $product = Product::where('id', $outputProduct->product_id)->first();
        $product->count = $product->count + $outputProduct->count;
        $product->update();
        return new FromResponse(true, "Muvafaqqiyatli bajarildi", 200);
    }
    public function calculateProductInputMinus(InputProduct $inputProduct): bool
    {
        $product = Product::where("shop_id", $inputProduct->shop_id)->where("id", $inputProduct->product_id)->first();
        if (!$product) {
            return false;
        }
        if (!$this->checkTotalCountOrPrice($product->count, $inputProduct->count && !$this->checkTotalCountOrPrice($product->price, $inputProduct->price))) {
            return false;
        }
        // dd($inputProduct);
        $inputProductTotalPrice = bcmul($this->checkOne($inputProduct->count ?? 1), $this->checkOne($inputProduct->price ?? 1), 6);
        $productTotalPrice = bcmul($this->checkOne($product->price??1), $this->checkOne($product->count??1), 6);
        $totalCount =  bcsub($product->count, $inputProduct->count, 6);
        if (($productTotalPrice - $inputProductTotalPrice) < 0 || $totalCount <= 0) {
            $product->price = 0;
        } else {
            $product->price = bcdiv(bcsub($productTotalPrice, $inputProductTotalPrice, 6), $totalCount, 6);
        }
        $product->count = $totalCount;
        $product->update();
        return true;
    }
    public function checkZero($args): float
    {
        if ($args) {
            dd($args,"1");
            return $args;
        } else {
            dd($args,"2");
            return 0;
        }
    }
    public function checkOne($args): float
    {
        if ($args && $args > 0) {
            return $args;
        } else {
            return 1;
        }
    }
    public function checkTotalCountOrPrice(int $args1, int $args2): bool
    {
        if ($args1 >= $args2) {
            return true;
        }
        return false;
    }
}
