<?php

namespace App\Repositories;

use App\Http\Requests\StoreProductRequest;
use App\Interfaces\ProductInterface;
use App\Models\Product;
use App\Models\Shop;

class ProductRepository implements ProductInterface
{
    public function getAllProductBySHope(int $shopId,string $name)
    {
        return Product::where('shop_id',$shopId)->where('name', 'like', "%".$name.'%')->orderBy("name","asc")->paginate(20);
        // return Product::where('shop_id',$shopId)->paginate(20);
    }
    public function getProductById(int $productId)
    {
        return Product::find($productId)->get();
    }
    public function deleteProduct(int $productId)
    {
        return Product::destroy($productId);
    }
    public function getProductsListByShopId(int $shopId, string $name)
    {
        return Product::where('shop_id',$shopId)->where('name', 'like', "%".$name.'%')->orderBy("name","asc")->paginate(20);
    }
    public function createProduct(StoreProductRequest $storeProductRequest)
    {
        return Product::create([
            "name" => $storeProductRequest->name,
            "shop_id"=>$storeProductRequest->shop_id,
            "category_id"=>$storeProductRequest->category_id,
        ]);
    }
    public function updateProduct(StoreProductRequest $storeProductRequest,Product $product)
    {
        
    }
}
