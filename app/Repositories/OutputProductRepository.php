<?php

namespace App\Repositories;

use App\Http\Requests\UpdateOutputProductRequest;
use App\Interfaces\OutputProductInterface;
use App\Models\OutputProduct;
use App\Models\OutputProductTotal;

class OutputProductRepository implements OutputProductInterface
{
    public function getAllOutputProductBySHope(int $shopId,string $startDate, string $endDate)
    {
        return OutputProductTotal::where("shop_id", $shopId)->whereBetween("updated_at", [$startDate, $endDate])->with('outputProducts.product')->orderBy("created_at","desc")->paginate(20);
    }
    public function getOutputProductById(int $OutputProductId)
    {
    }
    public function deleteOutputProduct(OutputProduct $product)
    {
        return $product->delete();
    }
    public function createOutputProduct($storeProductRequest, int $outputProductId, int $shopId)
    {
        // dd($storeProductRequest);
        $outputProduct = array();
        foreach ($storeProductRequest as $key => $value) {
            // dd($key);
            array_push($outputProduct, OutputProduct::create([
                "shop_id" => $shopId,
                "product_id" => $value["product_id"],
                "output_product_total_id" => $outputProductId,
                "count" => $value["count"],
                "client_name" => $value["client_name"],
                "client_phone_number" => $value["client_phone_number"],
                "username" => auth()->user()->username,
                "price" => $value["price"],
                "debt" => $value['debt'] < 1 ? false : true,
                "price_debt" => $value["price_debt"],
            ]));
        }
        return $outputProduct;
    }
    public function updateOutputProduct(UpdateOutputProductRequest $storeProductRequest, OutputProduct $product)
    {
        $product->update([
            "product_id" => $storeProductRequest->product_id,
            "count" => $storeProductRequest->count,
            "username" => auth()->user()->username,
            "price" => $storeProductRequest->price,
            "client_phone_number" => $storeProductRequest->client_phone_number,
            "debt" => $storeProductRequest->price_debt < 1 ? false : true,
            "price_debt" => $storeProductRequest->price_debt,
        ]);
        return $product;
    }
}
