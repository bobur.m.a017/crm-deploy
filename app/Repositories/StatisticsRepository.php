<?php

namespace App\Repositories;

use App\Interfaces\StatisticsInterface;
use App\Models\InputProduct;
use App\Models\OutputProduct;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class StatisticsRepository implements StatisticsInterface
{
    public function getToatalStatisticsBySHope(int $shopId, string $startDate, string $endDate)
    {
        return response()->json([
            "input" => InputProduct::where("shop_id", $shopId)->whereBetween("updated_at", [$startDate, $endDate])->sum("price"),
            "output" => OutputProduct::where("shop_id", $shopId)->whereBetween("updated_at", [$startDate, $endDate])->sum("price"),
            "debt_price_total" => OutputProduct::where("shop_id", $shopId)->whereBetween("updated_at", [$startDate, $endDate])->where("debt", "true")->where("price_debt", "!=", 0)->sum("price_debt"),
        ], 200);
    }
    public function getWeeklyStatisticsBySHope(int $shopeId,string $startDate,string $endDate)
    {
        
        return array(
            "input" => InputProduct::where("shop_id", $shopeId)->whereBetween("updated_at", [$startDate, $endDate])->select(DB::raw('SUM(count * price) as total_price'))->first()->total_price,
            "output" => OutputProduct::where("shop_id", $shopeId)->whereBetween("updated_at", [$startDate, $endDate])->select(DB::raw('SUM(count * price) as total_price'))->first()->total_price,
            "debt_price_total" => OutputProduct::where("shop_id", $shopeId)->whereBetween("updated_at", [$startDate, $endDate])->where("debt", "true")->where("price_debt", "!=", 0)->sum("price_debt"),
        );
    }
    public function getTopProductsStatisticsBySHope(int $shopId, string $startDate, string $endDate)
    {
        $topProducts = Product::select([
            'products.*',
            DB::raw('COUNT(output_products.id) as output_count'),
            DB::raw('SUM(output_products.count) as output_count_total')
        ])
        ->leftJoin('output_products', 'products.id', '=', 'output_products.product_id')
        ->where('products.shop_id', $shopId)
        ->whereBetween('output_products.created_at', [$startDate, $endDate])
        ->groupBy('products.id')
        ->orderByDesc('output_count')
        ->limit(20)
        ->get();
       return response()->json($topProducts, 200);
    }
    public function getDebtsStatisticsBySHope(int $shopId)
    {
    }
    public function getResidualsStatisticsBySHope(int $shopId)
    {
       $products = Product::where("shop_id", $shopId)->where("count", ">", 0)->get();
       $debt = OutputProduct::where("shop_id", $shopId)->where("debt", "true")->where("price_debt", "!=", 0)->get();
       return response()->json([
        "products" =>$products,
        "products_category_count" => $products->count(),
        "products_count" => $products->sum("count"),
        "product_prices_sum" => $products->sum("price")* $products->sum("count"),
        "product_prices_sum2" => Product::where("shop_id", $shopId)->where("count", ">", 0)->select(DB::raw('SUM(count * price) as total_price'))->first()->total_price,
        "debt" => $debt,
        "debt_count" => $debt->count(),
        "debt_sum" => $debt->sum("price_debt"),
        // 1646133138.42294
    ], 200);
    }
}
