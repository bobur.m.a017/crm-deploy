<?php

namespace App\Repositories;

use App\Http\Requests\StoreInputProductRequest;
use App\Http\Requests\UpdateInputProductRequest;
use App\Interfaces\InputProductInterface;
use App\Models\InputProduct;
use App\Models\Shop;
use Illuminate\Support\Facades\DB;

class InputProductRepository implements InputProductInterface
{
    public function getAllInputProductBySHope(int $shopId, string $start_date, string $end_date)
    {
        // return InputProduct::whereHas('product', function ($query) use ($shopId) {
        //     $query->where('shop_id', $shopId);
        // })->paginate(20);
        return Shop::find($shopId)->inputProducts()->whereBetween("updated_at", [$start_date, $end_date])->orderBy("updated_at","desc")->with('product')->paginate(20);
        // return InputProduct::where("shop_id",$shopeId)->paginate(10);
    }
    public function getInputProductById(int $inputProductId)
    {
    }
    public function deleteInputProduct(InputProduct $product)
    {
       return $product->delete();
    }
    public function createInputProduct(StoreInputProductRequest $storeProductRequest)
    {
        return InputProduct::create([
            "shop_id" => $storeProductRequest->shop_id,
            "product_id" => $storeProductRequest->product_id,
            "count" => $storeProductRequest->count,
            "username" => auth()->user()->username,
            "price" => $storeProductRequest->price,
        ]);
    }
    public function updateInputProduct(UpdateInputProductRequest $storeProductRequest, InputProduct $product)
    {
       $product->update([
            "shop_id" => $storeProductRequest->shop_id,
            "product_id" => $storeProductRequest->product_id,
            "count" => $storeProductRequest->count,
            "username" => auth()->user()->username,
            "price" => $storeProductRequest->price,
        ]);
        return $product;
    }
}
