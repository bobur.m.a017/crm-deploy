<?php

namespace App\Repositories;

use App\Http\Requests\StoreCategoryRequest;
use App\Interfaces\CategoryInterface;
use App\Models\Category;
use App\Models\Shop;

class CategroyRepository implements CategoryInterface
{
    public function getAllCategoryBySHope(int $shopId)
    {
        return Shop::with('categories.products')->find($shopId)->categories;
    }
    public function getCategoryById(int $categoryId)
    {
        return Category::find($categoryId)->get();
    }
    public function deleteCategory(int $categoryId)
    {
        return Category::destroy($categoryId);
    }
    public function createCategory(StoreCategoryRequest $storeCategoryRequest)
    {
        return Category::create([
            "name" => $storeCategoryRequest->name,
            "shop_id" => $storeCategoryRequest->shop_id,
        ]);
    }
    public function updateCategory(int $categoryId, string $name)
    {
        return Category::where('id', $categoryId)
            ->update(['name' => $name]);
    }
    public function getCategoryListByShopId(int $categoryId, string $search)
    {
        return Category::where('shop_id', $categoryId)
            ->where('name', 'like', '%' . $search . '%')
            ->orderBy("name","asc")
            ->paginate(20);
    }
}
