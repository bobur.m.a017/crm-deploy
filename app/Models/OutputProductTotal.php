<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class OutputProductTotal extends Model
{
    use HasFactory;
    protected $fillable = ['shop_id'];
    public function outputProducts() : HasMany {
        return $this->hasMany(OutputProduct::class);
    }
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
        
    }
}
