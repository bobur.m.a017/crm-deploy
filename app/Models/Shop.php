<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Shop extends Model
{
    use HasFactory;
    protected $fillable = ['name','user_id'];
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function categories(): HasMany
    {
        return $this->hasMany(Category::class);
    }
    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
    public function outputProducts(): HasMany
    {
        return $this->hasMany(OutputProduct::class);
    }
    public function inputProducts(): HasMany
    {
        return $this->hasMany(InputProduct::class);
    }
    public function outputProductTotals(): HasMany
    {
        return $this->hasMany(OutputProductTotal::class);
        
    }
}
