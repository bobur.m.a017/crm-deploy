<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OutputProduct extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'price',
        'count',
        'output_product_total_id',
        'username',
        'shop_id',
        "client_name",
        "client_phone_number",
        'debt',
        'price_debt',
        'product_id',
    ];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }
    public function outputProductTotal(): BelongsTo
    {
        return $this->belongsTo(OutputProductTotal::class);
    }
}
