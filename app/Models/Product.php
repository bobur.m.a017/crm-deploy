<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use HasFactory;
    protected $fillable = ['name','price','category_id','shop_id','count'];
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }
    public function inputProducts():HasMany
    {
       return $this->hasMany(InputProduct::class);
    }
    public function outputProducts():HasMany
    {
       return $this->hasMany(OutputProduct::class);
    }
}
