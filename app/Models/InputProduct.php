<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class InputProduct extends Model
{
    use HasFactory;
    protected $fillable = ['name','price','count','product_id','username','shop_id'];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }
}
