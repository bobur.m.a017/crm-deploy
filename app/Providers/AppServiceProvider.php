<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\InputProduct;
use App\Models\OutputProduct;
use App\Models\Product;
use App\Models\Shop;
use App\Models\User;
use App\Policies\CategoryPolicy;
use App\Policies\InputProductPolicy;
use App\Policies\OutputProductPolicy;
use App\Policies\ProductPolicy;
use App\Policies\ShopPolicy;
use App\Policies\UserPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Gate::policy(Category::class, CategoryPolicy::class);
        Gate::policy(Product::class, ProductPolicy::class);
        Gate::policy(OutputProduct::class, OutputProductPolicy::class);
        Gate::policy(InputProduct::class, InputProductPolicy::class);
        Gate::policy(User::class, UserPolicy::class);
        Gate::policy(Shop::class, ShopPolicy::class);
    }
}
