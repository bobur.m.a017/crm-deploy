<?php

namespace App\Providers;

use App\Interfaces\CategoryInterface;
use App\Interfaces\InputProductInterface;
use App\Interfaces\OutputProductInterface;
use App\Interfaces\ProductInterface;
use App\Interfaces\StatisticsInterface;
use App\Repositories\CategroyRepository;
use App\Repositories\InputProductRepository;
use App\Repositories\OutputProductRepository;
use App\Repositories\ProductRepository;
use App\Repositories\StatisticsRepository;
use Illuminate\Support\ServiceProvider;

class ProviderServicesRepository extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(StatisticsInterface::class,StatisticsRepository::class);
        $this->app->bind(InputProductInterface::class,InputProductRepository::class);
        $this->app->bind(OutputProductInterface::class,OutputProductRepository::class);
        $this->app->bind(ProductInterface::class,ProductRepository::class);
        $this->app->bind(CategoryInterface::class,CategroyRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
