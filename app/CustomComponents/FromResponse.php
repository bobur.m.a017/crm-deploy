<?php
namespace App\CustomComponents;
class FromResponse
{
    public $state;
    public $message;
    public $code;

    public function __construct($state, $message, $code)
    {
        $this->state = $state;
        $this->message = $message;
        $this->code = $code;
    }
}
