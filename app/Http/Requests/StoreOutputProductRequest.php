<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOutputProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "shop_id"=>"required",
            "output_products"=>"required|array|min:1",
            "output_products.*.count"=>"required",
            "output_products.*.price"=>"required",
            "output_products.*.price_debt"=>"required",
            "output_products.*.product_id"=>"required",
        ];
    }
}
