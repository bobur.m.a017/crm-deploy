<?php

namespace App\Http\Controllers;

use App\Http\Requests\TotalStatisticsRequest;
use App\Services\StatisticsServices;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
    public function __construct(protected StatisticsServices $services)
    {
    }

    public function getTotalStatistics(TotalStatisticsRequest $request)
    {
        // dd($request->shop_id);
        return $this->services->getTotalStatistics($request->shop_id, $request->start_date, $request->end_date);
    }
    public function getTopProducts(Request $request)
    {
        // dd($request->shop_id);
        return $this->services->getTopProductsStatisticsBySHope($request->shop_id, $request->start_date, $request->end_date);
    }
    public function getResiduals(Request $request)
    {
        // dd($request->shop_id);
        return $this->services->getResidualsStatisticsBySHope($request->shop_id);
    }

    public function getWeekly(Request $request)
    {
        if (!$request->month) {
            return response()->json([], 405);
        }
        list($year, $month) = explode("-", $request->month);
        return response()->json(
            $this->services->getWeeklyStatisticsBySHope($request->shop_id, (int)$year, (int)$month)
        );
    }
}
