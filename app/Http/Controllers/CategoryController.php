<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Services\CategoryServices;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryServices;
    public function __construct(CategoryServices $categoryServices,)
    {
        $this->categoryServices = $categoryServices;
    }
    public function getList(Request $request)
    {
        if ($request->query('shop_id')) {
            # code...
            $shopId = (int)$request->query('shop_id');
            // dd($shopId);
            return response()->json(
                $this->categoryServices->getCategoryListByShopId($shopId, $request->name ?? ""),
                200
            );
        }
        return response()->json("error qilishdi params yuq", 404);
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->query('shop_id')) {
            # code...
            $shopId = (int)$request->query('shop_id');
            // dd($shopId);
            return response()->json(
                $this->categoryServices->getCategories($shopId),
                200
            );
        } else {
            return response()->json("error qilishdi pamas yuq", 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCategoryRequest $request)
    {
        return response()->json($this->categoryServices->createCategory($request), 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->name = $request->name;
        return response()->json(
            $category->update(),
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return response()->json("ok", 200);
    }
}
