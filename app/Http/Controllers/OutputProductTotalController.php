<?php

namespace App\Http\Controllers;

use App\Models\OutputProductTotal;
use App\Http\Requests\StoreOutputProductTotalRequest;
use App\Http\Requests\UpdateOutputProductTotalRequest;

class OutputProductTotalController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreOutputProductTotalRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(OutputProductTotal $outputProductTotal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(OutputProductTotal $outputProductTotal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateOutputProductTotalRequest $request, OutputProductTotal $outputProductTotal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(OutputProductTotal $outputProductTotal)
    {
        //
    }
}
