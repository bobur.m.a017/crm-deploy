<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Services\ProductServices;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function __construct(protected ProductServices $productServices)
    {
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        return response()->json($this->productServices->getProducts($request->shop_id,$request->name ?? ""));
    }
    public function getList(Request $request)
    {
        if ($request->query('shop_id')) {
            # code...
            $shopId = (int)$request->query('shop_id');
            // dd($shopId);
            return response()->json(
                $this->productServices->getProductsListByShopId($shopId, $request->name ?? ""),
                200
            );
        }
        return response()->json("error qilishdi params yuq", 404);
        
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProductRequest $request)
    {
        // return request()->json("sssssss");
        // dd($request);
        return response()->json(
            $this->productServices->createProduct($request),200
        );
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        $product->name = $request->name;
        return response()->json(
            $product->save()
            ,200
        );
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        return response()->json(
            $product->delete()
            ,200
        );
    }
}
