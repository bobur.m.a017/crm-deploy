<?php

namespace App\Http\Controllers;

use App\Models\OutputProduct;
use App\Http\Requests\StoreOutputProductRequest;
use App\Http\Requests\UpdateOutputProductRequest;
use App\Services\OutputProductServices;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OutputProductController extends Controller
{
    public function __construct(protected OutputProductServices $outputProductServices)
    {
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $today = Carbon::now();
        $monthAgo = Carbon::now()->subMonth();
        return $this->outputProductServices->getAllOutputProductBySHope($request->shop_id, $request->start_date ?? $monthAgo, $request->end_date ?? $today);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreOutputProductRequest $request)
    {
        $reponse = $this->outputProductServices->createOutputProduct($request);
        return response()->json($reponse, $reponse->code);
    }

    /**
     * Display the specified resource.
     */
    public function show(OutputProduct $outputProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(OutputProduct $outputProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateOutputProductRequest $request, OutputProduct $outputProduct)
    {
        $reponse = $this->outputProductServices->updateOutputProduct($request, $outputProduct);

        return response()->json($reponse, $reponse->code);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(OutputProduct $outputProduct)
    {

        $isDeleted = $this->outputProductServices->deleteOutputProduct($outputProduct);
        return response()->json($isDeleted ? "Muvaffaqiyatli" : "Muvaffaqiyatsizlik", $isDeleted ? 200 : 500);
    }
}
