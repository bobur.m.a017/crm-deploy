<?php

namespace App\Http\Controllers;

use App\Models\InputProduct;
use App\Http\Requests\StoreInputProductRequest;
use App\Http\Requests\UpdateInputProductRequest;
use App\Services\InputProductServices;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InputProductController extends Controller
{
    public function __construct(protected InputProductServices $inputProductService)
    {
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        // dd($request->shop_id);
        $today = Carbon::now();
        $monthAgo = Carbon::now()->subMonth();
        return response()->json(
            $this->inputProductService->getAllInputProductBySHope($request->shop_id, $request->start_date ?? $monthAgo, $request->end_date ?? $today),
            200
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreInputProductRequest $request)
    {
        if ($this->inputProductService->createInputProduct($request)) {
            return response()->json("Muvaffaqiyatli", 200);
        }
        return response()->json("Muvaffaqiyatsizlik", 500);
    }

    /**
     * Display the specified resource.
     */
    public function show(InputProduct $inputProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(InputProduct $inputProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateInputProductRequest $storeProductRequest, InputProduct $inputProduct)
    {
        // return response()->json([
        //     "shop_id" => $storeProductRequest->shop_id,
        //     "product_id" => $storeProductRequest->product_id,
        //     "count" => $storeProductRequest->count,
        //     "username" => auth()->user()->username,
        //     "price" => $storeProductRequest->price,
        // ], 200);
        if ($this->inputProductService->updateInputProduct($storeProductRequest, $inputProduct)) {
            return response()->json("Muvaffaqiyatli", 200);
        }
        return response()->json("Muvaffaqiyatsizlik", 500);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(InputProduct $inputProduct)
    {
        if ($this->inputProductService->deleteInputProduct($inputProduct)) {
            return response()->json("Muvaffaqiyatli", 200);
        }
        return response()->json("Muvaffaqiyatsizlik", 500);
    }
}
