/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./public/index.php",
    "./resources/**/*.{js,jsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}