<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\InputProductController;
use App\Http\Controllers\OutputProductController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\StatisticController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::get('user', function (Request $request) {
    return response()->json(
        ['user' => auth()->user(), 'shops_users' => auth()->user()->shops]
    );
})->middleware('auth:sanctum');

Route::post('login', [AuthController::class, 'login']);
Route::get('statistic/total', [StatisticController::class, 'getTotalStatistics'])->middleware('auth:sanctum');
Route::get('statistic/top', [StatisticController::class, 'getTopProducts'])->middleware('auth:sanctum');
Route::get('statistic/residual', [StatisticController::class, 'getResiduals'])->middleware('auth:sanctum');
Route::get('statistic/week', [StatisticController::class, 'getWeekly'])->middleware('auth:sanctum');
Route::get('category/list', [CategoryController::class, 'getList'])->middleware('auth:sanctum');
Route::get('product/list', [ProductController::class, 'getList'])->middleware('auth:sanctum');

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::apiResources([
        'category' => CategoryController::class,
        'input-product' => InputProductController::class,
        'output-product' => OutputProductController::class,
        'product' => ProductController::class,
    ]);
});
